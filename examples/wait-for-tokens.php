<?php

require(__DIR__.'/common.php');

use Nucleardog\RateLimit\RateLimit;

// Create a rate limiter that allows 3 tokens every second
$rateLimit = RateLimit::perSecond(3);

// Start consuming tokens
for ($i=0;;$i++)
{
	// Wait until a token is available, consume it, and run the callback to
	// "generate" some value.
	$value = $rateLimit->with(1, function() {
		return microtime();
	});
	echo sprintf('[%8d] %s', $i, $value).PHP_EOL;
}

// When run, this script will:
// - Output nothing for one second, because the rate limiter starts empty and
//   needs a second to accumulate some tokens.
// - Has 3 tokens available, so immediately outputs three values in quick
//   succession, exhausting the rate limit.
// - Waits one more second for more tokens to accumulate.
// - Has 3 tokens available, output, etc.
// - Repeat forever.