<?php

require(__DIR__.'/common.php');

use Nucleardog\RateLimit\RateLimit;

// Create a rate limiter that allows 3 tokens every second
$rateLimit = RateLimit::perSecond(3);

for ($i=0;;$i++)
{
	echo sprintf('[%8d] %s', $i, 'Tick').PHP_EOL;

	while ($rateLimit->has(1))
	{
		$rateLimit->use(1);
		echo "           Nom nom nom.".PHP_EOL;
	}

	usleep(500000);
}
