<?php

require(__DIR__.'/common.php');

use Nucleardog\RateLimit\RateLimit;
use Nucleardog\RateLimit\Exceptions\RateLimitExceededException;

// Create a rate limiter that allows 3 tokens every second
$rateLimit = RateLimit::perSecond(3);

for ($i=0;;$i++)
{
	echo sprintf('[%8d] %s', $i, 'Tick').PHP_EOL;

	try
	{
		$rateLimit->use(1);
		echo "           Nom nom nom.".PHP_EOL;
	}
	catch (RateLimitExceededException)
	{
		// Small backoff when we exceed the rate limit.
		usleep(100000);
	}
}
