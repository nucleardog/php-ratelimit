<?php

declare(strict_types=1);

namespace Nucleardog\RateLimit\Exceptions;

class RateLimitExceededException extends RateLimitException
{

}
