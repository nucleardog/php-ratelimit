<?php

declare(strict_types=1);

namespace Nucleardog\RateLimit;
use Closure;

class RateLimit
{
	private const MICROSECONDS_PER_SECOND = 1000000;

	/**
	 * Last time the tick method ran to accumulate time/tokens, in microseconds
	 * @var int
	 */
	private int $lastTick;

	/**
	 * Every time we tick, we accumulate the microseconds since last tick here.
	 * When this exceeds the interval, then we can start adding more tokens.
	 * @var int
	 */
	private int $accumulator;

	/**
 	 * Interval at which tokens should be added to the rate limit, in microseconds.
 	 * @var int
 	 */
	private int $interval;

	/**
	 * How many tokens are added each interval
	 * @var int
	 */
	private int $rate;

	/**
 	 * Maximum capacity for the rate limiter; we can never accumulate more than
 	 * this many tokens.
 	 * @var int
	 */
	private int $capacity;

	/**
 	 * Current value of the rate limiter; zero is empty and means no actions
 	 * can be taken. Should be in the range 0 <-> $capacity.
	 */
	private int $tokens;

	/**
 	 * @param int $tokens how many tokens to add each interval
 	 * @param int $interval interval to refresh at, in microseconds
 	 * @param int $initial initial number of tokens available
 	 * @param ?int $capacity how many tokens can be accumulated; null to match $tokens; use PHP_INT_MAX for "no" limit
	*/
	public function __construct(
		int $tokens,
		int $interval,
		int $initial = 0,
		?int $capacity = null,
	) {
		// Initialize time tracking
		$this->lastTick = static::getTimestamp();
		$this->accumulator = 0;

		// Initialize token counts and rates
		$this->rate = $tokens;
		$this->interval = $interval;
		$this->tokens = $initial;
		$this->capacity = $capacity ?? $tokens;
	}

	/**
 	 * Create a new rate limiter with a given rate per second
 	 *
 	 * @param int $tokens number of tokens accumulated every second
 	 * @param int $initial initial number of tokens available
 	 * @param ?int $capacity maximum tokens accumulated; null to match $tokens; use PHP_INT_MAX for "no" limit
	 */
	public static function perSecond(int $tokens, int $initial = 0, ?int $capacity = null): RateLimit
	{
		return new RateLimit(
			tokens: $tokens,
			interval: static::secondsToMicroseconds(1),
			initial: $initial,
			capacity: $capacity,
		);
	}

	/**
 	 * Create a new rate limiter with a given rate per minute
 	 *
 	 * @param int $tokens number of tokens accumulated every minute
 	 * @param int $initial initial number of tokens available
 	 * @param ?int $capacity maximum tokens accumulated; null to match $tokens; use PHP_INT_MAX for "no" limit
	 */
	public static function perMinute(int $tokens, int $initial = 0, ?int $capacity = null): RateLimit
	{
		return new RateLimit(
			tokens: $tokens,
			interval: static::secondsToMicroseconds(60),
			initial: $initial,
			capacity: $capacity,
		);
	}

	/**
	 * Refill tokens up to capacity
	 *
	 * @return self
	 */
	public function fill(): RateLimit
	{
		$this->tokens = $this->capacity;
		return $this;
	}

	/**
	 * Consume all tokens
	 *
	 * @return self
	 */
	public function empty(): RateLimit
	{
		$this->tokens = 0;
		return $this;
	}

	/**
	 * Set the current number of tokens available
	 *
	 * @param int $tokens
	 * @return self
	 */
	public function set(int $tokens): RateLimit
	{
		$this->tokens = $tokens;
		return $this;
	}

	/**
 	 * Accumulate tokens based on time passed
 	 *
 	 * @return void
	 */
	public function tick(): void
	{
		// Accumulate how much time has passed since last tick
		$ts = static::getTimestamp();
		$this->accumulator += $ts - $this->lastTick;
		$this->lastTick = $ts;

		// See how many full intervals are in the accumulator
		$intervals = (int)floor($this->accumulator / $this->interval);

		// Add tokens based on the number of intervals that have passed, consuming
		// time from the accumulator.
		$this->accumulator -= $intervals * $this->interval;
		$this->tokens += (int)min($intervals * $this->rate, $this->capacity);
	}

	/**
	 * Wait until a number of tokens are available then execute the callback
	 *
	 * @param int $tokens
	 * @param Closure $callback
	 * @return mixed value returned by callback
	 */
	public function with(int $tokens, Closure $callback): mixed
	{
		while ($this->tokens < $tokens) {
			// Figure out how many more tokens we need
			$need = $tokens - $this->tokens;
			// Calculate how many intervals would need to elapse to accumulate that many more.
			$intervals = (int)ceil($need / $this->rate);
			// Calculate how long we have to wait for that many intervals to pass,
			// accounting for currently accumulated time.
			$wait = ($intervals * $this->interval) - $this->accumulator;
			// Now wait that long.
			usleep($wait);
			// Tick to accumulate tokens; this should get us up to the required capacity.
			$this->tick();
		}

		// We have the required number of tokens available. Use them.
		$this->use($tokens);

		// Now run the requested code.
		return $callback();
	}

	/**
	 * Consume the specified number of tokens from the rate limit
	 *
	 * @throws Exceptions\RateLimitExceededException
	 * @param int $tokens
	 * @return void
	 */
	public function use(int $tokens): void
	{
		$this->tick();
		if ($this->tokens < $tokens) {
			throw new Exceptions\RateLimitExceededException('Rate limit exceeded');
		}

		$this->tokens -= $tokens;
	}

	/**
	 * Determine if the given number of tokens are available
	 *
	 * @param int $count
	 * @return bool
	 */
	public function has(int $count): bool
	{
		$this->tick();
		return $this->tokens >= $count;
	}

	/**
	 * Fetch the current timestamp as an integer number of microseconds
	 *
	 * @return int
	 */
	private static function getTimestamp(): int
	{
		return static::secondsToMicroseconds(microtime(true));
	}

	/**
	 * Convert a given number of seconds or fractional seconds to microseconds.
	 *
	 * This truncates any precision greater than microseconds.
	 *
	 * @param float $seconds
	 * @return int
	 */
	private static function secondsToMicroseconds(float $seconds): int
	{
		return (int)($seconds * static::MICROSECONDS_PER_SECOND);
	}
}